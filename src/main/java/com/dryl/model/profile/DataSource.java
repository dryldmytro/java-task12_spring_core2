package com.dryl.model.profile;

import org.springframework.stereotype.Component;

@Component
public class DataSource {
    private String url;
    private String user;

    public DataSource(String url, String user) {
        this.url = url;
        this.user = user;
    }

    @Override
    public String toString() {
        return "DataSource{" +
                "url='" + url + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
