package com.dryl;

import com.dryl.model.anotherpackage.BeanInSetter;
import com.dryl.model.anotherpackage.BeanPrimary;
import com.dryl.model.config.SecondConfig;
import com.dryl.model.config.ThirdConfig;
import com.dryl.model.profile.ConfigOne;
import com.dryl.model.profile.ConfigTwo;
import com.dryl.model.profile.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ThirdConfig.class);
        BeanInSetter bis = context.getBean(BeanInSetter.class);
        bis.print();
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME,"two");
        ApplicationContext context1 = new AnnotationConfigApplicationContext(ConfigTwo.class);
        DataSource ds = context1.getBean(DataSource.class);
        System.out.println(ds);
    }
}
