package com.dryl.model.anotherpackage;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("beanB")
@Order(1)
@Scope("singleton")
public class OtherBeanB implements OtherBean{
    String name = "other bean B";

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "OtherBeanB{" +
                "name='" + name + '\'' +
                '}';
    }
}
