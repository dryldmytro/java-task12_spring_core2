package com.dryl.model.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("two")
@ComponentScan("com.dryl.profile")
public class ConfigTwo {
    @Bean
    public DataSource getData(){
        return new DataSource("localhost","Admin");
    }
}
