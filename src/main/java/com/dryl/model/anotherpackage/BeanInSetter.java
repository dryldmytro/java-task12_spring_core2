package com.dryl.model.anotherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BeanInSetter {
    @Autowired
    private List<OtherBean>list;
    private OtherBean a;
    private OtherBean b;
    @Autowired
    @Qualifier("beanC")
    private OtherBean c;

    @Autowired
    public BeanInSetter(@Qualifier("beanA") OtherBeanA a) {
        this.a = a;
    }

  public void print(){
        list.add(a);
      list.add(b);
      list.add(c);
      for (OtherBean o:list) {
          System.out.println(o.toString()+" = "+o.hashCode());
      }
  }

    @Autowired
    @Qualifier("beanB")
    public void setB(OtherBean b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "BeanInSetter{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
