package com.dryl.model.anotherpackage;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("beanA")
@Order(3)
@Primary
@Scope("prototype")
public class OtherBeanA implements OtherBean {
    String name = "Other bean A";

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "OtherBeanA{" +
                "name='" + name + '\'' +
                '}';
    }
}
