package com.dryl.model.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("one")
@ComponentScan("com.dryl.profile")
public class ConfigOne {
    @Bean
    public DataSource getData(){
        return new DataSource("8810","First");
    }
}
