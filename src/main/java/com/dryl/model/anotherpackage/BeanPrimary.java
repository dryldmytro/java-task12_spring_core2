package com.dryl.model.anotherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanPrimary {
    @Autowired
    OtherBean a;
    @Autowired
    @Qualifier("beanB")
    OtherBean b;
    @Autowired
    OtherBean c;

    @Override
    public String toString() {
        return "BeanPrimary{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
