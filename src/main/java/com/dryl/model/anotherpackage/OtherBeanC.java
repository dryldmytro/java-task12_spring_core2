package com.dryl.model.anotherpackage;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("beanC")
@Order(2)
@PropertySource("project.properties")
@Scope("prototype")
public class OtherBeanC implements OtherBean {
    @Value("${C.name}")
    String name;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "name='" + name + '\'' +
                '}';
    }
}
