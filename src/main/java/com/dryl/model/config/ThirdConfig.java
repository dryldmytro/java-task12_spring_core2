package com.dryl.model.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dryl.model.anotherpackage")
public class ThirdConfig {
}
