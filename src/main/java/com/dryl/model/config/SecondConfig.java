package com.dryl.model.config;

import com.dryl.model.beans1.BeanB;
import com.dryl.model.beans2.CatAnimal;
import com.dryl.model.beans3.BeanD;
import com.dryl.model.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(value = "com.dryl.model.beans2", useDefaultFilters = false,
includeFilters = @ComponentScan.Filter(type = FilterType.REGEX,pattern = ".*Flower.*"))
@ComponentScan(value = "com.dryl.model.beans3",
includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,classes = {BeanD.class, BeanF.class}))
public class SecondConfig {
}
